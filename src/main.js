// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import './css/main.css'
import './css/iconfont.css'
import './css/border.css'
import './css/reset.css'
import 'mint-ui/lib/style.css'
import 'mint-ui/lib/swipe/style.css'
import 'swiper/dist/css/swiper.css'

Vue.config.productionTip = false

import axios from 'axios'
Vue.use(axios)
//axios.defaults.timeout = 10000;//响应时间
//axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';//配置请求头
//axios.defaults.baseURL = 'https://api.douban.com/v2/movie/';   //配置接口地址
Vue.prototype.$axios = axios;

//1195d55bc86476db8c6d44cf255168c5
import fastclick from 'fastclick'
fastclick.attach(document.body)

import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper)

import Mint from 'mint-ui'
Vue.use(Mint)

//import xwPop from './components/third/xxPop/js/xwPop.js'
import './components/third/xxPop/css/xwPop.css'
//Vue.use(xwPop)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
