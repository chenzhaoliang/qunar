import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'
import Me from '@/components/Me'
import MovieHot from '@/components/MovieHot'
import City from '@/components/City'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
    },
    {
      path: '/me',
      name: 'Me',
      component: Me
    },
    {
      path:'/hot',
      name: 'hot',
      component: MovieHot
    },
    {
        path:'/city',
        name: 'city',
        component: City
    }
  ]
})
